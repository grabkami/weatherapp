package com.quaigon.weatherapp.services.citiesprovider;

import com.quaigon.weatherapp.data.City;

import java.util.List;

/**
 * Created by Kamil on 28.05.2017.
 */

public interface CitiesProvider {
    List<City> provideCities();
}
