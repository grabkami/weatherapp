package com.quaigon.weatherapp.services.citiesprovider;

import com.quaigon.weatherapp.data.City;
import com.quaigon.weatherapp.data.weather.Coord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 28.05.2017.
 */

public class DummyCitiesProvider implements CitiesProvider {

    private List<City> cities;

    public DummyCitiesProvider() {
        this.cities = new ArrayList<>();

        cities.add(new City("Cracow",new Coord(50.06143, 19.93658)));
        cities.add(new City("Warsaw",new Coord(52.2297, 21.0122)));
        cities.add(new City("Poznan", new Coord(52.4166667, 16.9666667)));
        cities.add(new City("Gdansk", new Coord(54.3520, 18.6466)));
        cities.add(new City("Zakopane", new Coord(49.29899, 19.94885)));
    }

    @Override
    public List<City> provideCities() {
        return cities;
    }
}
