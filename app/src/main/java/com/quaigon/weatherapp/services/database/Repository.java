package com.quaigon.weatherapp.services.database;

import java.util.List;

/**
 * Created by Kamil on 29.05.2017.
 */

public interface Repository <T>{

    void add (T item);
    void delete(long id);
    List<T> getAll();

    T findById(long id);
}
