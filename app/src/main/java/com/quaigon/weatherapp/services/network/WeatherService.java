package com.quaigon.weatherapp.services.network;

import com.quaigon.weatherapp.data.weather.Weather;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Kamil on 28.05.2017.
 */

public interface WeatherService {

    @GET("weather/?appid=" + NetworkModule.appid +"&")
    Observable<Weather> getWeather (@Query("lat") double lat, @Query("lon") double lon);
}
