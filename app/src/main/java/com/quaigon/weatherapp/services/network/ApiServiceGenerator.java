package com.quaigon.weatherapp.services.network;

import retrofit2.Retrofit;

/**
 * Created by Kamil on 28.05.2017.
 */

public class ApiServiceGenerator {

    private final Retrofit retrofit;

    public ApiServiceGenerator(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    public <T> T createService(Class<T> serviceClass) {
        return retrofit.create(serviceClass);
    }
}
