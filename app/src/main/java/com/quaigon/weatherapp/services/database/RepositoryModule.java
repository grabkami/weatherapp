package com.quaigon.weatherapp.services.database;

import com.quaigon.weatherapp.data.CityWeather;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Kamil on 29.05.2017.
 */

@Module
public class RepositoryModule {

    @Provides
    Repository<CityWeather> provideWeatherRepository() {
        return new RealmWeatherRepository();
    }
}
