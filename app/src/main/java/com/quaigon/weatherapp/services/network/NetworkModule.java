package com.quaigon.weatherapp.services.network;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Kamil on 28.05.2017.
 */

@Module
public class NetworkModule {

    private static final String baseUrl = "http://api.openweathermap.org/data/2.5/";
    public static final String appid = "908b73eda93e7a8bf07d2ce6ddb90abd";

    @Provides
    @Singleton
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor loggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    ApiServiceGenerator provideApiServiceGenerator(Retrofit retrofit) {
        return new ApiServiceGenerator(retrofit);
    }

    @Provides
    @Singleton
    WeatherService provideWeatherService(ApiServiceGenerator apiServiceGenerator) {
        return apiServiceGenerator.createService(WeatherService.class);
    }
}
