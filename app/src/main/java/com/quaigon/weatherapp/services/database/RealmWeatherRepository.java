package com.quaigon.weatherapp.services.database;

import com.quaigon.weatherapp.data.CityWeather;

import java.util.List;

import io.realm.Realm;

/**
 * Created by Kamil on 29.05.2017.
 */

public class RealmWeatherRepository implements Repository <CityWeather>{

    private static final long MAX_COUNT = 5;
    private static final String PRIMARY_KEY = "primary_key";

    @Override
    public void add(CityWeather item) {
        Realm realm = Realm.getDefaultInstance();

        realm.executeTransactionAsync(realm1 -> {
            int key;
            try {
                key = realm1.where(CityWeather.class).max(PRIMARY_KEY).intValue() + 1;
            } catch (Exception ex) {
                key = 0;
            }
            if (realm1.where(CityWeather.class).count() >= MAX_COUNT) {
                List<CityWeather> results = realm1.where(CityWeather.class).findAllSorted(PRIMARY_KEY);
                results.get(0).deleteFromRealm();
            }
            item.setPrimary_key(key);
            realm1.copyToRealmOrUpdate(item);
        }, realm::close);

    }

    @Override
    public void delete(long id) {
        Realm realm = Realm.getDefaultInstance();

        realm.where(CityWeather.class).equalTo(PRIMARY_KEY, id).findAll().deleteAllFromRealm();

        realm.close();
    }

    @Override
    public List<CityWeather> getAll() {
        Realm realm = Realm.getDefaultInstance();

        List<CityWeather> weatherList = realm.copyFromRealm(realm.where(CityWeather.class).findAllAsync());

        realm.close();

        return weatherList;
    }

    @Override
    public CityWeather findById(long id) {
        Realm realm = Realm.getDefaultInstance();

        CityWeather cityWeather = realm.where(CityWeather.class).equalTo("primary_key", id).findFirst();

        CityWeather copied = realm.copyFromRealm(cityWeather);

        realm.close();

        return copied;
    }
}
