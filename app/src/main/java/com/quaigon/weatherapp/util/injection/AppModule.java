package com.quaigon.weatherapp.util.injection;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.quaigon.weatherapp.util.base.App;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Kamil on 28.05.2017.
 */

@Module
public class AppModule {

    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    App provideApplication() {
        return app;
    }

    @Provides
    Context provideApplicationContext() {
        return app.getApplicationContext();
    }

    @Provides
    SharedPreferences provideSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(app);
    }
}
