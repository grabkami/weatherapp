package com.quaigon.weatherapp.util.injection;

import android.content.Context;
import android.content.SharedPreferences;

import com.quaigon.weatherapp.data.CityWeather;
import com.quaigon.weatherapp.services.database.Repository;
import com.quaigon.weatherapp.services.database.RepositoryModule;
import com.quaigon.weatherapp.util.base.App;
import com.quaigon.weatherapp.services.network.ApiServiceGenerator;
import com.quaigon.weatherapp.services.network.NetworkModule;
import com.quaigon.weatherapp.services.network.WeatherService;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Kamil on 28.05.2017.
 */

@Singleton
@Component(modules = {
        AppModule.class,
        NetworkModule.class,
        RepositoryModule.class
})
public interface AppComponent {
    App myApp();
    Context appContext();
    SharedPreferences sharedPrefs();

    ApiServiceGenerator apiServiceGenerator();

    Repository<CityWeather> repository();
    WeatherService weather();
}
