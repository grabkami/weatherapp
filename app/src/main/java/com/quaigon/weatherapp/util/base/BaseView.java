package com.quaigon.weatherapp.util.base;

/**
 * Created by Kamil on 28.05.2017.
 */

public interface BaseView {
    void showMessegeDialog(String messege);
    void showProgressDialog();
    void hideProgressDialog();
}
