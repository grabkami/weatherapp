package com.quaigon.weatherapp.util.injection;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Kamil on 28.05.2017.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface RuntimeScope {
}

