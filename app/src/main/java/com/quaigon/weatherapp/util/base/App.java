package com.quaigon.weatherapp.util.base;

import android.app.Application;

import com.quaigon.weatherapp.util.injection.AppComponent;
import com.quaigon.weatherapp.util.injection.AppModule;
import com.quaigon.weatherapp.util.injection.DaggerAppComponent;

import io.realm.Realm;
import lombok.Getter;
import timber.log.Timber;

/**
 * Created by Kamil on 28.05.2017.
 */

public class App extends Application {

    @Getter
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initRealm();
        initDagger();
        initTimber();
    }

    private void initTimber() {
        Timber.plant(new Timber.DebugTree());
    }

    private void initDagger() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    private void initRealm() {
        Realm.init(this);
    }
}
