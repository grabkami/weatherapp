package com.quaigon.weatherapp.util.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.quaigon.weatherapp.R;

import butterknife.ButterKnife;

/**
 * Created by Kamil on 28.05.2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    ProgressDialog progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());

        ButterKnife.bind(this);
        initDagger();
        progress = new ProgressDialog(this);
    }

    public abstract int getLayoutId();

    public abstract void initDagger();

    protected App getApp() {
        return (App) getApplication();
    }

    @Override
    public void showMessegeDialog(String messege) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle(R.string.error)
                .setMessage(messege)
                .setNegativeButton(R.string.ok, (dialog, which) -> dialog.dismiss())
                .show();
    }

    @Override
    public void showProgressDialog() {
        progress.setMessage(getString(R.string.loading));
        progress.show();
    }

    @Override
    public void hideProgressDialog() {
        progress.dismiss();
    }

}
