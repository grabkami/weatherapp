package com.quaigon.weatherapp.util.base;

import io.reactivex.annotations.NonNull;

/**
 * Created by Kamil on 28.05.2017.
 */

public interface BasePresenter <T extends BaseView> {
    void setView(@NonNull  T view );
    void clearView();
}
