package com.quaigon.weatherapp.data;

import com.google.android.gms.maps.model.LatLng;
import com.quaigon.weatherapp.data.weather.Coord;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

import io.realm.RealmObject;

import static org.parceler.Parcel.Serialization;

/**
 * Created by Kamil on 28.05.2017.
 */

@Parcel(value = Serialization.BEAN)
public class City extends RealmObject {
    String name;
    Coord position;

    @ParcelConstructor
    public City(String name, Coord position) {
        this.name = name;
        this.position =  position;
    }

    public String getName() {
        return name;
    }

    public Coord getPosition() {
        return position;
    }

    public City() {
    }
}
