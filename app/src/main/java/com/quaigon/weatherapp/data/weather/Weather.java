package com.quaigon.weatherapp.data.weather;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

import io.realm.RealmObject;

/**
 * Created by Kamil on 28.05.2017.
 */

@Parcel
public class Weather extends RealmObject {
    Coord coord;
    WeatherMain main;

    @ParcelConstructor
    public Weather(Coord coord, WeatherMain main) {
        this.coord = coord;
        this.main = main;
    }

    public Weather() {
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public WeatherMain getMain() {
        return main;
    }

    public void setMain(WeatherMain main) {
        this.main = main;
    }
}
