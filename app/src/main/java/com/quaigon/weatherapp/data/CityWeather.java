package com.quaigon.weatherapp.data;

import com.quaigon.weatherapp.data.weather.Weather;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by kamilgrabowski on 29/05/17.
 */

@Parcel
public class CityWeather extends RealmObject {
    @PrimaryKey
    private long primary_key;
    private City city;
    private Weather weather;

    @ParcelConstructor
    public CityWeather(long primary_key, City city, Weather weather) {
        this.primary_key = primary_key;
        this.city = city;
        this.weather = weather;
    }

    public long getPrimary_key() {
        return primary_key;
    }

    public City getCity() {
        return city;
    }

    public Weather getWeather() {
        return weather;
    }

    public CityWeather() {
    }

    public CityWeather(City city, Weather weather) {
        this.city = city;
        this.weather = weather;
    }

    public void setPrimary_key(long primary_key) {
        this.primary_key = primary_key;
    }
}
