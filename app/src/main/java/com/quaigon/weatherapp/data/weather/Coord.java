package com.quaigon.weatherapp.data.weather;

import org.parceler.Parcel;

import io.realm.RealmObject;

/**
 * Created by Kamil on 28.05.2017.
 */

@Parcel
public class Coord extends RealmObject{

    double lat;
    double lon;

    public Coord(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public Coord() {
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }
}
