package com.quaigon.weatherapp.data.weather;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

import io.realm.RealmObject;

/**
 * Created by Kamil on 28.05.2017.
 */

@Parcel
public class WeatherMain extends RealmObject {

    private static final double KELVIN = 273.15;

    @SerializedName("temp")
    Double temperature;

    @SerializedName("temp_max")
    Double maxTemperature;

    @SerializedName("temp_min")
    Double minTemperature;

    @SerializedName("humidity")
    Double humidity;

    @ParcelConstructor
    public WeatherMain(Double temperature, Double maxTemperature, Double minTemperature, Double humidity) {
        this.temperature = temperature;
        this.maxTemperature = maxTemperature;
        this.minTemperature = minTemperature;
        this.humidity = humidity;
    }

    public WeatherMain() {
    }

    public Double getTemperature() {
        return temperature - KELVIN;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getMaxTemperature() {
        return maxTemperature - KELVIN;
    }

    public void setMaxTemperature(Double maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public Double getMinTemperature() {
        return minTemperature - KELVIN;
    }

    public void setMinTemperature(Double minTemperature) {
        this.minTemperature = minTemperature;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }
}
