package com.quaigon.weatherapp.ui.list.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.quaigon.weatherapp.R;
import com.quaigon.weatherapp.data.CityWeather;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kamilgrabowski on 29/05/17.
 */

public class LastSavedAdapter extends RecyclerView.Adapter<LastSavedViewHolder> {

    private List<CityWeather> weatherList;

    public LastSavedAdapter() {
        this.weatherList = new ArrayList<>();
    }

    @Override
    public LastSavedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_last_saved, parent, false);

        return new LastSavedViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LastSavedViewHolder holder, int position) {
        holder.loadData(weatherList.get(position));
    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }

    public void loadData(List<CityWeather> weatherList) {
        this.weatherList.clear();
        this.weatherList.addAll(weatherList);
        notifyDataSetChanged();
    }
}
