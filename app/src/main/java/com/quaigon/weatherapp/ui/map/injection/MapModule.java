package com.quaigon.weatherapp.ui.map.injection;

import com.quaigon.weatherapp.data.CityWeather;
import com.quaigon.weatherapp.services.database.Repository;
import com.quaigon.weatherapp.ui.map.MapPresenter;
import com.quaigon.weatherapp.services.citiesprovider.CitiesProvider;
import com.quaigon.weatherapp.services.citiesprovider.DummyCitiesProvider;
import com.quaigon.weatherapp.services.network.WeatherService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Kamil on 28.05.2017.
 */

@Module
public class MapModule {

    @Provides
    MapContract.Presenter providePresenter(CitiesProvider citiesProvider, WeatherService weatherService, Repository<CityWeather> repository) {
        return new MapPresenter(citiesProvider, weatherService, repository);
    }

    @Provides
    CitiesProvider provideCitiesProvider() {
        return new DummyCitiesProvider();
    }

}
