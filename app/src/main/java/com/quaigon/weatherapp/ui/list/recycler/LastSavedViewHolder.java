package com.quaigon.weatherapp.ui.list.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.quaigon.weatherapp.R;
import com.quaigon.weatherapp.data.CityWeather;
import com.quaigon.weatherapp.ui.list.LastSavedRxBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by kamilgrabowski on 29/05/17.
 */

public class LastSavedViewHolder extends RecyclerView.ViewHolder{

    @BindView(R.id.city_name_textview)
    TextView cityNameTextView;

    @BindView(R.id.temperature_textview)
    TextView temperatureTextView;

    private CityWeather cityWeather;

    public LastSavedViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void loadData(CityWeather cityWeather) {
        this.cityWeather = cityWeather;

        cityNameTextView.setText(cityWeather.getCity().getName());
        temperatureTextView.setText(String.valueOf(cityWeather.getWeather().getMain().getTemperature()));
    }

    @OnClick(R.id.container)
    void onContainerClicked() {
        LastSavedRxBus.getInstance().send(cityWeather.getPrimary_key());
    }


}
