package com.quaigon.weatherapp.ui.list;

import com.quaigon.weatherapp.data.CityWeather;
import com.quaigon.weatherapp.services.database.Repository;
import com.quaigon.weatherapp.ui.list.injection.LastSavedContract;

import java.util.List;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.realm.Realm;
import lombok.RequiredArgsConstructor;

/**
 * Created by kamilgrabowski on 29/05/17.
 */

@RequiredArgsConstructor
public class LastSavedPresenter implements LastSavedContract.Presenter{

    private LastSavedContract.View view;
    private Disposable weatherEventSub;
    private final Repository<CityWeather> cityWeatherRepository;

    @Override
    public void setView(@NonNull LastSavedContract.View view) {
        this.view = view;
        weatherEventSub = LastSavedRxBus.getInstance()
                .getEvent()
                .subscribe(id -> {
                    CityWeather cityWeather = cityWeatherRepository.findById(id);

                    view.showCityDetails(cityWeather.getCity(), cityWeather.getWeather());
                });
    }

    @Override
    public void clearView() {
        this.view = null;
        weatherEventSub.dispose();
    }

    @Override
    public void onCreated() {

        List<CityWeather> weatherList  = cityWeatherRepository.getAll();
        view.showWeatherList(weatherList);
    }
}
