package com.quaigon.weatherapp.ui;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quaigon.weatherapp.R;
import com.quaigon.weatherapp.data.City;
import com.quaigon.weatherapp.data.weather.Weather;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kamil on 28.05.2017.
 */

public class WeatherDetailsDialog extends DialogFragment {

    private static final String CITY = "city";
    private static final String WEATHER = "weather";

    @BindView(R.id.city_name_textview)
    TextView cityTextView;

    @BindView(R.id.temperature_textview)
    TextView temperatureTextView;

    @BindView(R.id.min_temperature_textview)
    TextView minTemperatureTextView;

    @BindView(R.id.max_temperature_textview)
    TextView maxTemperatureTextView;

    @BindView(R.id.humidity_textview)
    TextView humidityTextView;

    private City city;
    private Weather weather;

    public static WeatherDetailsDialog newInstance(City city, Weather weather) {

        Bundle args = new Bundle();

        args.putParcelable(CITY, Parcels.wrap(city));
        args.putParcelable(WEATHER, Parcels.wrap(weather));

        WeatherDetailsDialog fragment = new WeatherDetailsDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.weather_deatails_fragment, container, false);

        ButterKnife.bind(this, v);

        this.city = Parcels.unwrap(getArguments().getParcelable(CITY));
        this.weather = Parcels.unwrap(getArguments().getParcelable(WEATHER));

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        cityTextView.setText(city.getName());
        temperatureTextView.setText(String.valueOf(weather.getMain().getTemperature()));
        minTemperatureTextView.setText(String.valueOf(weather.getMain().getMinTemperature()));
        maxTemperatureTextView.setText(String.valueOf(weather.getMain().getMaxTemperature()));
        humidityTextView.setText(String.valueOf(weather.getMain().getHumidity()));
    }
}
