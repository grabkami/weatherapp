package com.quaigon.weatherapp.ui.list;

import io.reactivex.subjects.PublishSubject;

/**
 * Created by kamilgrabowski on 29/05/17.
 */

public class LastSavedRxBus {

    private static LastSavedRxBus instance;

    private PublishSubject<Long> cityWeatherPublishSubject = PublishSubject.create();

    public static LastSavedRxBus getInstance() {
        if (instance == null) {
            instance = new LastSavedRxBus();
        }

        return instance;
    }

    public void send(long cityWeather) {
        cityWeatherPublishSubject.onNext(cityWeather);
    }

    public PublishSubject<Long> getEvent() {
        return cityWeatherPublishSubject;
    }

}
