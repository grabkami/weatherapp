package com.quaigon.weatherapp.ui.list.injection;

import com.quaigon.weatherapp.ui.list.LastSavedActivity;
import com.quaigon.weatherapp.util.injection.AppComponent;
import com.quaigon.weatherapp.util.injection.RuntimeScope;

import dagger.Component;

/**
 * Created by kamilgrabowski on 29/05/17.
 */

@RuntimeScope
@Component(
        dependencies = AppComponent.class,
        modules = LastSavedModule.class
)
public interface LastSavedComponent {
    void inject(LastSavedActivity activity);
}
