package com.quaigon.weatherapp.ui.map.injection;

import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.quaigon.weatherapp.data.City;
import com.quaigon.weatherapp.data.weather.Weather;
import com.quaigon.weatherapp.util.base.BasePresenter;
import com.quaigon.weatherapp.util.base.BaseView;

import java.util.List;

/**
 * Created by Kamil on 28.05.2017.
 */

public interface MapContract {

    interface View extends BaseView {

        void zoomToLocation(LatLng cracowLatLang, int basicZoomValue);

        void showCities(List<City> cities);

        void displayWeather(City city, Weather weather);

        void zoomToLocation(CameraPosition cameraPosition);
    }

    interface Router {

    }

    interface Presenter extends BasePresenter<View> {

        void onMapReady();

        void onMarkerClicked(City city);

        void onSavedInstance(CameraPosition cameraPosition);
    }
}
