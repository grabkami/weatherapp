package com.quaigon.weatherapp.ui.map;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.quaigon.weatherapp.R;
import com.quaigon.weatherapp.data.City;
import com.quaigon.weatherapp.data.weather.Weather;
import com.quaigon.weatherapp.ui.WeatherDetailsDialog;
import com.quaigon.weatherapp.ui.list.LastSavedActivity;
import com.quaigon.weatherapp.ui.map.injection.DaggerMapComponent;
import com.quaigon.weatherapp.ui.map.injection.MapContract;
import com.quaigon.weatherapp.ui.map.injection.MapModule;
import com.quaigon.weatherapp.util.base.BaseActivity;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Kamil on 28.05.2017.
 */

public class MapActivity extends BaseActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, MapContract.View {

    private GoogleMap googleMap;

    @Inject
    MapContract.Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null ) {
            CameraPosition cameraPosition = savedInstanceState.getParcelable("position");
            presenter.onSavedInstance(cameraPosition);
        }
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.clearView();
    }

    private void init() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        presenter.setView(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.simple_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.list:
                Intent intent = new Intent(this, LastSavedActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_map;
    }

    @Override
    public void initDagger() {
        DaggerMapComponent.builder()
                .appComponent(getApp().getAppComponent())
                .mapModule(new MapModule())
                .build()
                .inject(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setOnMarkerClickListener(this);

        presenter.onMapReady();
    }

    @Override
    public void zoomToLocation(LatLng latLng, int basicZoomValue) {
        CameraUpdate center=
                CameraUpdateFactory.newLatLng(latLng);
        CameraUpdate zoom=CameraUpdateFactory.zoomTo(basicZoomValue);

        googleMap.moveCamera(center);
        googleMap.animateCamera(zoom);
    }

    @Override
    public void zoomToLocation(CameraPosition cameraPosition) {
        CameraUpdate center=
                CameraUpdateFactory.newCameraPosition(cameraPosition);

        googleMap.moveCamera(center);
    }

    @Override
    public void showCities(List<City> cities) {
        for (City city : cities) {
            Marker marker = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(city.getPosition().getLat(), city.getPosition().getLon()))
                    .title(city.getName()));
            marker.setTag(city);
        }
    }

    @Override
    public void displayWeather(City city, Weather weather) {
        WeatherDetailsDialog weatherDetailsDialog = WeatherDetailsDialog.newInstance(city, weather);
        weatherDetailsDialog.show(getFragmentManager(), " WeatherDialog");
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.getTitle();
        City city = (City) marker.getTag();
        presenter.onMarkerClicked(city);
        return false;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (googleMap != null) {
            outState.putParcelable("position", googleMap.getCameraPosition());
        }
        super.onSaveInstanceState(outState);
    }
}
