package com.quaigon.weatherapp.ui.list.injection;

import com.quaigon.weatherapp.data.City;
import com.quaigon.weatherapp.data.CityWeather;
import com.quaigon.weatherapp.data.weather.Weather;
import com.quaigon.weatherapp.util.base.BasePresenter;
import com.quaigon.weatherapp.util.base.BaseView;

import java.util.List;

/**
 * Created by kamilgrabowski on 29/05/17.
 */

public interface LastSavedContract {
    interface View extends BaseView {

        void showWeatherList(List<CityWeather> weatherList);

        void showCityDetails(City city, Weather weather);
    }

    interface Presenter extends BasePresenter<View> {

        void onCreated();

    }
}
