package com.quaigon.weatherapp.ui.list.injection;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.quaigon.weatherapp.data.CityWeather;
import com.quaigon.weatherapp.services.database.Repository;
import com.quaigon.weatherapp.ui.list.recycler.LastSavedAdapter;
import com.quaigon.weatherapp.ui.list.LastSavedPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by kamilgrabowski on 29/05/17.
 */

@Module
public class LastSavedModule {

    private Context context;

    public LastSavedModule(Context context) {
        this.context = context;
    }

    @Provides
    LastSavedContract.Presenter providePresenter(Repository<CityWeather> repository) {
        return new LastSavedPresenter(repository);
    }

    @Provides
    LastSavedAdapter provideAdapter() {
        return new LastSavedAdapter();
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        return linearLayoutManager;
    }
}
