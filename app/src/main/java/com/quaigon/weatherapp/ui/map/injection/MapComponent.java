package com.quaigon.weatherapp.ui.map.injection;

import com.quaigon.weatherapp.ui.map.MapActivity;
import com.quaigon.weatherapp.util.injection.AppComponent;
import com.quaigon.weatherapp.util.injection.RuntimeScope;

import dagger.Component;

/**
 * Created by Kamil on 28.05.2017.
 */

@RuntimeScope
@Component(
        dependencies = AppComponent.class,
        modules = MapModule.class
)
public interface MapComponent {
    void inject (MapActivity activity);
}
