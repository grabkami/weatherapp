package com.quaigon.weatherapp.ui.map;

import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.quaigon.weatherapp.data.City;
import com.quaigon.weatherapp.data.CityWeather;
import com.quaigon.weatherapp.data.weather.Weather;
import com.quaigon.weatherapp.services.database.Repository;
import com.quaigon.weatherapp.ui.map.injection.MapContract;
import com.quaigon.weatherapp.services.citiesprovider.CitiesProvider;
import com.quaigon.weatherapp.services.network.WeatherService;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import lombok.RequiredArgsConstructor;
import retrofit2.HttpException;
import timber.log.Timber;

/**
 * Created by Kamil on 28.05.2017.
 */

@RequiredArgsConstructor
public class MapPresenter implements MapContract.Presenter {

    private static final long MAX_COUNT = 5;
    private static LatLng cracowLatLang = new LatLng(50.06143, 19.93658);
    private static int basicZoomValue = 9;
    private final CitiesProvider citiesProvider;
    private final WeatherService weatherService;
    private final Repository<CityWeather> weatherRepository;

    private MapContract.View view;
    private CameraPosition cameraPosition;

    @Override
    public void setView(MapContract.View view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        this.view = null;
    }

    @Override
    public void onMapReady() {

        if (cameraPosition == null) {
            view.zoomToLocation(cracowLatLang, basicZoomValue);
        } else {
            view.zoomToLocation(cameraPosition);
        }

        view.showCities(citiesProvider.provideCities());
    }

    @Override
    public void onMarkerClicked(City city) {
        view.showProgressDialog();
        Observable<Weather> weatherObservable = weatherService.getWeather(city.getPosition().getLat(), city.getPosition().getLon());

        weatherObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(() -> view.hideProgressDialog())
                .subscribe(weather -> {
                    view.displayWeather(city, weather);

                    saveLastClick(city, weather);
                }, throwable -> {
                    Timber.e(throwable);
                    if (throwable instanceof HttpException) {
                        view.showMessegeDialog("Networl error occured");
                    }
                });
    }

    @Override
    public void onSavedInstance(CameraPosition cameraPosition) {
        this.cameraPosition = cameraPosition;
    }

    private void saveLastClick(City city, Weather weather) {
        weatherRepository.add(new CityWeather(city, weather));
    }

}
