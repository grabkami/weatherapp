package com.quaigon.weatherapp.ui.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.quaigon.weatherapp.R;
import com.quaigon.weatherapp.data.City;
import com.quaigon.weatherapp.data.CityWeather;
import com.quaigon.weatherapp.data.weather.Weather;
import com.quaigon.weatherapp.ui.WeatherDetailsDialog;
import com.quaigon.weatherapp.ui.list.injection.DaggerLastSavedComponent;
import com.quaigon.weatherapp.ui.list.injection.LastSavedContract;
import com.quaigon.weatherapp.ui.list.injection.LastSavedModule;
import com.quaigon.weatherapp.ui.list.recycler.LastSavedAdapter;
import com.quaigon.weatherapp.util.base.BaseActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by kamilgrabowski on 29/05/17.
 */


public class LastSavedActivity extends BaseActivity implements LastSavedContract.View {

    @BindView(R.id.last_saved_recycler)
    RecyclerView lastSavedRecyclerView;

    @Inject
    LastSavedContract.Presenter presenter;

    @Inject
    LastSavedAdapter lastSavedAdapter;

    @Inject
    LinearLayoutManager linearLayoutManager;

    @Override
    public int getLayoutId() {
        return R.layout.activity_last_saved;
    }

    @Override
    public void initDagger() {
        DaggerLastSavedComponent.builder()
                .appComponent(getApp().getAppComponent())
                .lastSavedModule(new LastSavedModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setView(this);

        init();
    }

    private void init() {
        lastSavedRecyclerView.setLayoutManager(linearLayoutManager);
        lastSavedRecyclerView.setAdapter(lastSavedAdapter);

        presenter.onCreated();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.clearView();
    }

    @Override
    public void showWeatherList(List<CityWeather> weatherList) {
        lastSavedAdapter.loadData(weatherList);
    }

    @Override
    public void showCityDetails(City city, Weather weather) {
        WeatherDetailsDialog weatherDetailsDialog = WeatherDetailsDialog.newInstance(city, weather);
        weatherDetailsDialog.show(getFragmentManager(), "Details");
    }
}
